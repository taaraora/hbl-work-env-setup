import test from 'ava';
import {pushToStorage} from './tictactoe';
import localStorage from 'mock-local-storage'

test('throws exception if moves are spoiled', t => {
  t.throws(()=>{
    pushToStorage(null, null);
  }, 'moves need to be an array');
});
