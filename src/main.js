import generateRows from "./generateField";
import start from "./tictactoe"

const field = document.querySelector('.field');

generateRows(field, 3);

start(field);

