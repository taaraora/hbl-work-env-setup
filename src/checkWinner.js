export function getWinningRowIndex(field) {
  const rows = Object.values(field.querySelectorAll('.row'));
  let winRow = null;

  rows.forEach((row, ind) => {
    const cells = Object.values(row.childNodes);
    const rowWon = cells.every(cell => cell.classList.contains('ch')) || cells.every(cell => cell.classList.contains('r'));
    if (rowWon) {
      winRow = ind;
    }
  });

  return winRow;
}

export function getWinningColumnIndex(field, N) {
  for (let i = 0; i < N; i += 1) {
    const cellsCh = [];
    const cellsR = [];
    for (let j = 0; j < N; j += 1) {
      const cell = field.querySelector(`#c-${(j * N) + i}`);
      cellsCh.push(cell.classList.contains('ch'));
      cellsR.push(cell.classList.contains('r'));
    }
    if (cellsCh.every(e => e)) {
      return i;
    }
    if (cellsR.every(e => e)) {
      return i;
    }
  }
  return null;
}

export function checkFirstDiagonale(field, N) {
  const cellsCh = [];
  const cellsR = [];
  for (let i = 0; i < N * N; i += N + 1) {
    const cell = field.querySelector(`#c-${i}`);
    cellsCh.push(cell.classList.contains('ch'));
    cellsR.push(cell.classList.contains('r'));
  }
  if (cellsCh.every(e => e)) {
    return true;
  }
  if (cellsR.every(e => e)) {
    return true;
  }
  return null;
}

export function checkSecondDiagonale(field, N) {
  const cellsCh = [];
  const cellsR = [];
  for (let i = N - 1; i < (N * N) - 1; i += N - 1) {
    const cell = field.querySelector(`#c-${i}`);
    cellsCh.push(cell.classList.contains('ch'));
    cellsR.push(cell.classList.contains('r'));
  }
  if (cellsCh.every(e => e)) {
    return true;
  }
  if (cellsR.every(e => e)) {
    return true;
  }
  return null;
}

export function checkWin(field, N) {
  const rowIndex = getWinningRowIndex(field);
  const columnIndex = getWinningColumnIndex(field, N);
  const firstDiag = checkFirstDiagonale(field, N);
  const secondDiag = checkSecondDiagonale(field, N);

  return columnIndex !== null || rowIndex !== null || firstDiag || secondDiag;
}
