import test from 'ava';
import equals from './equals';

test('undefined and null return false', (t) => {
  t.false(equals(undefined, null))
});